<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStudentRequest;
use App\Student;

class HomeController extends Controller
{
    public function index()
    {
        $students = Student::query()->orderBy('number')->paginate(8);
        /** @var Student|null $topStudent */
        $topStudent = null;
        /** @var Student|null $bottomStudent */
        $bottomStudent = null;

        $letters = [
            "A" => 0,
            "B" => 0,
            "C" => 0,
            "D" => 0,
        ];

        foreach (Student::all() as $student) {
            if (!$topStudent || $topStudent->success < $student->success)
                $topStudent = $student;
            if (!$bottomStudent || $bottomStudent->success > $student->success)
                $bottomStudent = $student;
            if (!isset($letters[$student->letter]))
                $letters[$student->letter] = 1;
            else
                $letters[$student->letter] += 1;
        }

        return view('home.index', [
            'students' => $students,
            'letters' => $letters,
            'topStudent' => $topStudent,
            'bottomStudent' => $bottomStudent,
        ]);
    }

    public function store(CreateStudentRequest $request)
    {
        Student::create($request->only('number', 'name', 'surname', 'midterm', 'finale') + ['success' => $request->midterm*.4 + $request->finale*.6]);

        return redirect()->back();
    }
}
