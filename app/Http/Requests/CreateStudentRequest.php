<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "number" => "required|unique:students",
            "name" => "required|min:3",
            "surname" => "required|min:3",
            "midterm" => "required|min:0|max:100|numeric",
            "finale" => "required|min:0|max:100|numeric",
        ];
    }
}
