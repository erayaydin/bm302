<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'number', 'name', 'surname', 'success', 'midterm', 'finale',
    ];

    public function getLetterAttribute() {
        if ($this->success > 75)
            return "A";
        if ($this->success > 50)
            return "B";
        if ($this->success > 25)
            return "C";
        return "D";
    }

    public function getFullNameAttribute() {
        return $this->name." ".$this->surname;
    }
}
