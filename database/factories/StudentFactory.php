<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    $midterm = $faker->numberBetween(0, 100);
    $finale = $faker->numberBetween(0, 100);
    $success = $midterm*.4 + $finale*.6;

    return [
        'number' => $faker->numerify('##-######-##'),
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'midterm' => $midterm,
        'finale' => $finale,
        'success' => $success,
    ];
});
