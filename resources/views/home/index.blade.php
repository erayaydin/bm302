<!DOCTYPE html>
<html>
<head>
    <title>BM302 - İnternet Programlama</title>
    <meta charset="utf-8">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
<div class="container" style="margin-top: 10px;">
    <div class="row" style="margin-bottom: 30px;">
        <div class="col-md-12">
            <h1 class="text-center">BM302 - İnternet Programlama</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Yeni Kayıt</h3>
                </div>
                <div class="panel-body">
                    @if($errors->count() > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif

                    <form method="post" id="register" action="{{ route('store') }}">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <input type="text" class="form-control" name="number" value="{{ old('number') }}" placeholder="Öğrenci No" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Ad" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="surname" value="{{ old('surname') }}" placeholder="Soyad" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="midterm" value="{{ old('miderm') }}" placeholder="Vize Notu" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="finale" value="{{ old('finale') }}" placeholder="Final Notu" />
                        </div>
                        <button type="submit" class="btn btn-success btn-block">Ekle</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <table class="table table-responsive table-bordered table-striped" id="list">
                <thead>
                <tr>
                    <th>Öğrenci No</th>
                    <th>Ad Soyad</th>
                    <th>Vize Notu</th>
                    <th>Final Notu</th>
                    <th>Başarı Notu</th>
                    <th>Harf</th>
                </tr>
                </thead>
                <tbody>
                @foreach($students as $student)
                    <tr>
                        <td>{{ $student->number }}</td>
                        <td>{{ $student->fullName }}</td>
                        <td>{{ $student->midterm }}</td>
                        <td>{{ $student->finale }}</td>
                        <td>{{ $student->success }}</td>
                        <td>{{ $student->letter }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $students->render() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Harf Dağılımı</h3>
                </div>
                <div class="panel-body">
                    <canvas id="letterChart" width="400" height="400"></canvas>

                    <table class="table table-bordered table-striped table-hover" id="letters">
                        <tbody>
                        @foreach ($letters as $letter => $count)
                            <tr>
                                <th>{{ $letter }}</th>
                                <td>{{ $count }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">En Yüksek Başarı Notuna Sahip Öğrenci</h3>
                </div>
                <div class="panel-body">
                    <div id="topStudent">
                        <h3 class="text-center">{{ $topStudent->fullName }}</h3>
                        <p class="text-center"><span class="label label-success" style="font-size: 24px; line-height: 55px;">{{ $topStudent->success }}</span></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">En Düşük Başarı Notuna Sahip Öğrenci</h3>
                </div>
                <div class="panel-body">
                    <div id="bottomStudent">
                        <h3 class="text-center">{{ $bottomStudent->fullName }}</h3>
                        <p class="text-center"><span class="label label-danger" style="font-size: 24px; line-height: 55px;">{{ $bottomStudent->success }}</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script>
    var ctx = document.getElementById('letterChart').getContext('2d');
    var letterChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [@foreach ($letters as $letter => $count) '{{ $letter }}', @endforeach],
            datasets: [{
                label: 'Harf',
                data: [
                    @foreach ($letters as $letter => $count) '{{ $count }}', @endforeach
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
</script>
</body>
</html>
